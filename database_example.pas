program Database_Example;

uses WebTestDb, sqldb, sqlite3conn;

{
 Make database with `sqlite3 test.db` with:

 create table names (
 id integer, name varchar(100)
 );
 insert into names (id, name) values(1, 'Brian');
 insert into names (id, name) values(2, 'Britt');
}

var
  Connection: TSQLite3Connection;
  QueryRec: TQueryRec;
begin
    Connection := CreateConnection('test.db');
    QueryRec := RunQuery(Connection, 'select * from names');
    while not QueryRec.Query.EOF do
    begin
      WriteLn('ID: ', QueryRec.Query.FieldByName('id').AsInteger, ' ',
              'Name: ', QueryRec.Query.FieldByName('name').AsString);
      QueryRec.Query.Next;
    end;

    CloseAndFreeQueryRec(QueryRec);
end.
