program Template;

{$mode objfpc}{$H+}{$J-}

uses SimpleTemplate;

var
  Data: TTemplateDictionary;
begin
  WriteLn('Test');
  Data := TTemplateDictionary.Create;
  Data.Add('test', 'this is a test');
  Data.Add('foobar', 'FooBar');
  WriteLn(RenderTemplate('Kode Pos {{test}} ({{foobar}})', Data));

  WriteLn(UseT('tmpl/postData.html'));
end.
