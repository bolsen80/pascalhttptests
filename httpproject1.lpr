program httpproject1;

{$mode objfpc}{$H+}{$M+}
// $M+ is needed for connecting fpjsonrtti to streaming system

uses
 {$ifdef UNIX}
cthreads, cmem,
    {$endif}
sysutils,
classes,
fphttpapp,
httpdefs,
httproute,
httpprotocol,
SimpleTemplate,
fptemplate,
inifiles,
fpjson,
fpjsonrtti;

type
  SettingsRec = class(TPersistent)
    private
    FHostname: String;
    FUsername: String;
    FPassword: String;
    FTimeout: integer;
  published
    property hostname: String read FHostname write FHostname;
    property Username: String read FUsername write FUsername;
    property Password: String read FPassword write FPassword;
    property Timeout: Integer read FTimeout write FTimeout;
  end;

  TReplaceTag = class
    procedure ReplaceTag(Sender: TObject; const TagString:String;
                         TagParams: TStringList; Out ReplaceText: String);
  end;

procedure route1(aReq: TRequest; aResp: TResponse);
begin
  aResp.Content := '<html><body><h1>Route 1 The Default</h1></body></html>';
end;

procedure route2(aReq: TRequest; aResp: TResponse);
begin
  aResp.Content := '<html><body><h1>Route 2</h1></body></html>';
end;

procedure routeGet(aReq: TRequest; aResp: TResponse);

var
  Name: String;
  Body: String;
begin

  Name := aReq.QueryFields.Values['name'];
  Name := httpprotocol.HTTPEncode(Name);
  if Name <> '' Then
    Body := 'Route 3 for ' + Name
  else
    Body := 'Route 3 The Default';

  aResp.Content := '<html><body><h1>' + Body + '</h1></body></html>';
end;

procedure withTemplate(aReq: TRequest; aResp: TResponse);

var
  Tmpl: String;
  Data: TTemplateDictionary;
begin
  Tmpl := '<html><body><h1> {{message}} </h1></body></html>';
  Data := TTemplateDictionary.Create;
  Data.Add('message', 'withTemplate Route');
  aResp.Content := RenderTemplate(Tmpl, Data);
end;

procedure TReplaceTag.ReplaceTag(Sender: TObject; const TagString: String; TagParams: TStringList; out ReplaceText: String);
var
  Headers:array[1..2] of String;
  HeaderStr : String;
begin
  if AnsiCompareText(TagString, 'DateTime') = 0 then
  begin
    ReplaceText := FormatDateTime(TagParams.Values['FORMAT'], Now);
  end
  else if AnsiCompareText(TagString, 'ReportResult') = 0 then
  begin
    Headers[1] := 'Name';
    Headers[2] := 'Age';
    HeaderStr := StringReplace(TagParams.Values['Header'], '~Column1', Headers[1], []);
    HeaderStr := StringReplace(HeaderStr, '~Column2', Headers[2], []);
    ReplaceText := HeaderStr;

    ReplaceText := ReplaceText + TagParams.Values['Footer'];
  end;
end;

procedure withFpTemplate(aReq: TRequest; aResp: TResponse);

// A useful source of info is in fpc example source:
// fpcsrc/packages/fcl-web/examples/fptemplate/listrecords/webmodule/webmodule.pas
//
// basically, the `ReplaceTag` function is called multiple times whenever it meets a {+ +} block.
// The logic happens at the `ReplaceTag` procedure.
//
   // It is implemented within a method of a class because the type declaration adds `of object` at the end
   // of the procedure, which says that it is a method of class it needs.

   // All programmatic functionality happens in ReplaceTag, not the template.
   // This is probably OK as the template language doesn't have to be sophisticated.
var
  Tmpl: TFPTemplate;
  RTag: TReplaceTag;
begin
  Tmpl := TFPTemplate.Create;
  Tmpl.FileName := 'tmpl/fpTemplate.html';
  Tmpl.AllowTagParams := true;
  Tmpl.StartDelimiter := '<%';
  Tmpl.EndDelimiter := '%>';
  Tmpl.ParamStartDelimiter := '{{';
  Tmpl.ParamEndDelimiter := '}}';
  RTag := TReplaceTag.Create; // this needs to be freed?
  Tmpl.OnReplaceTag := @RTag.ReplaceTag;

  aResp.Content := Tmpl.GetContent;
end;

procedure postData(aReq: TRequest; aResp: TResponse);

var
  Tmpl: String;
  Data: TTemplateDictionary;
begin
  Tmpl := UseT('tmpl/postData.html');
  Data := TTemplateDictionary.Create;
  if aReq.Method = 'GET' Then
    Data.Add('name', 'there!');

  if aReq.Method = 'POST' Then
    begin
      Data.Add('name', httpprotocol.HTTPEncode(aReq.ContentFields.Values['name']));
    end;

  aResp.Content := RenderTemplate(Tmpl, Data);
end;

procedure iniValues(aReq: TRequest; aResp: TResponse);
// https://wiki.freepascal.org/Using_INI_Files
// requires inifiles
// https://wiki.freepascal.org/Streaming_JSON (on serializing to objects)
// requires fpjson, fpjsonrtti
var
  settings: TIniFile;
  settRecord : SettingsRec;
  jsonSerialize: TJSONStreamer;
  objectContent: String;
begin
  settings := TIniFile.Create('settings.ini');
  settRecord := SettingsRec.Create;
  settRecord.Hostname := Settings.ReadString('General', 'hostname', '127.0.0.1');
  settRecord.Username := Settings.ReadString('General', 'username', '');
  settRecord.Password := Settings.ReadString('General', 'password', '');
  settRecord.Timeout := Settings.ReadInteger('General', 'timeout', 1);

  try
    jsonSerialize := TJSONStreamer.Create(nil);
    aResp.ContentType := 'application/json';
    objectContent := jsonSerialize.ObjectToJSONString(settRecord);
  finally
    FreeAndNil(jsonSerialize);
    FreeAndNil(settings);
    FreeAndNil(settRecord);
  end;
  aResp.Content := objectContent;
end;

begin
  HTTPRouter.RegisterRoute('/', @route1, True);
  HTTPRouter.RegisterRoute('/secondRoute', @route2);
  HTTPRouter.RegisterRoute('/getParam', @routeGet);
  HTTPRouter.RegisterRoute('/withTemplate', @withTemplate);
  HTTPRouter.RegisterRoute('/fpTemplate', @withFpTemplate);
  HTTPRouter.RegisterRoute('/postData', TRouteMethod.rmGet, @postData, False);
  HTTPRouter.RegisterRoute('/postData/finish', TRouteMethod.rmPost, @postData, False);
  HTTPRouter.RegisterRoute('/showIniValues', @iniValues, False);

  Application.Port := 8080;
  Application.Threaded := true;
  Application.Initialize;
  Application.run;
end.
