unit SimpleTemplate;

{$mode objfpc}{$H+}

interface

uses Classes, SysUtils, Kumis, Generics.Collections;

type 
  TTemplateDictionary = specialize TDictionary<String, String>;

function UseT(const AFileName: String): String;
function GetSection(const AName: string; const Iterator: array of const; Data: Pointer): boolean;
function GetVariable(const AName: string; const Iterator: array of const; Data: Pointer): string;
function RenderTemplate(template: String; Data: TTemplateDictionary) : String;

implementation

function UseT(const AFileName: String): String;

var
  TemplateFile: TFileStream;
begin
  try
    TemplateFile := TFileStream.Create(AFileName, fmOpenRead);
    SetLength(Result, TemplateFile.Size);
    TemplateFile.Read(Result[1], TemplateFile.Size);
  finally
    FreeAndNil(TemplateFile);
end;
end;

function GetSection(const AName: string; const Iterator: array of const; Data: Pointer): boolean;
begin
  Result := True;
end;

function GetVariable(const AName: string; const Iterator: array of const; Data: Pointer): string;

var
  TmplData: TTemplateDictionary;
begin
  TmplData := TTemplateDictionary(Data);
  Result := TmplData.GetItem(AName);
end;

function RenderTemplate(template: String; Data: TTemplateDictionary) : String;

var
  Tmpl: TKumisElArr;
begin
  Tmpl := Parse(template);
  Result := Render(Tmpl, @GetSection, @GetVariable, Data);
end;

begin
end.
