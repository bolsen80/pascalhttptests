unit WebTestDb;

{$mode objfpc}{$H+}

interface

uses
  Classes, sysutils, sqldb, sqlite3conn, inifiles;

type
  TQueryRec = record
    Connection: TSQLite3Connection;
    Transaction: TSQLTransaction;
    Query: TSQLQuery;
  end;

function CreateConnection(ADatabase: string): TSQLite3Connection;
function RunQuery(AConnection: TSQLite3Connection; AQueryText: string): TQueryRec;
procedure CloseAndFreeQueryRec(QueryRec: TQueryRec);

implementation

function CreateConnection(ADatabase: string): TSQLite3Connection;
begin
  Result := TSQLite3Connection.Create(nil);
  Result.DatabaseName := ADatabase;
  WriteLn(Result.DatabaseName);
end;

function RunQuery(AConnection: TSQLite3Connection; AQueryText: string): TQueryRec;
var
  ATransaction: TSQLTransaction;
  AQuery: TSQLQuery;
begin
  ATransaction := TSQLTransaction.Create(AConnection);
  AConnection.Transaction := ATransaction;
  AConnection.Open;
  ATransaction.StartTransaction;

  AQuery := TSQLQuery.Create(nil);
  AQuery.DataBase := AConnection;
  AQuery.SQL.Text := AQueryText;
  AQuery.Open;


  Result.Connection := AConnection;
  Result.Transaction := ATransaction;
  Result.Query := AQuery;
end;

procedure CloseAndFreeQueryRec(QueryRec: TQueryRec);
begin
  QueryRec.Query.Close;
  QueryRec.Connection.Close;

  FreeAndNil(QueryRec.Query);
  FreeAndNil(QueryRec.Transaction);
  FreeAndNil(QueryRec.Connection);

end;

begin
end.
