# HTTP tests in Pascal

The purpose of this is to write lots of samples of common web development things in Pascal for demonstration and inclusion in an eventual tutorial.

So far what's implemented:

 - [X] Routing with `httproute`
 - [X] Basic output
 - [X] Display value from query string, encoded.
 - [X] Render a template with [Kumis](https://github.com/TandangDewe/kumis). (Source is included until I switch to a git submodule.)
 - [X] Wrap code around Kumis to implement basic string-only data for Kumis.
 - [X] Write more complex wrapper to support other data-types.
 - [X] Post a form and show the result in the same template.
 - [X] Familiarize with the ini module
 - [X] Implement some database procedures just to see things work.
 - [ ] Test [in-process connection pooling module with PostgreSQL](https://github.com/ronaldobim/PascalDBPoolConnection).
 - [ ] Test HTTP sample with various database connections - no pooling, in-process pooling, out-of-process pooling with PgBouncer.
 - [ ] Post a form and redirect to a view of that data (by storing the result in the equivalent of a "flash")
 - [ ] Implement a general session for users, with a basic authentication scheme. Store in SQLite or Redis.
 - [ ] Implement CSRF protection (OWASP's description of stateless, in-form, using HMAC/SipHash for also validating if it is 'ours')
 - [ ] Write a processing-type job, with two endpoints: one to send, the other to collect the result.
 - [ ] Do typical things with HTTP headers (control cache, etc.)
 - [ ] Implement a simple form that stores and lists data from an SQL table.
 - [ ] Implement a form with CSRF protection.
 - [ ] Create a CLI interface to specify server details.
 - [X] Create a JSON API using `fpjson`.
 - [ ] Write a CLI client with `TFPHTTPClient`.
 - [ ] Implement same client as above with `Indy` or others.
 - [ ] Implement access logs and other application-level logger
 - [ ] Try to do the above with mORMot
 - [ ] Test the code with FpUnit. (Or FPCUnit)
 - [ ] Wrap C libraries for Prometheus to send statistics from application

Other dev related things:
 - [ ] Separate sample code into units.
 - [ ] Auto-format source
 - [-] Make a MkDocs site.
 - [ ] Use `git submodule` to link to Kumis.
 - [ ] Check for memory leaks by adding the appropriate flags into a build configuration.
 - [ ] Write a CLI to generate a scaffold.
 - [ ] Write a inotify file watcher to auto-compile on file changes (FPC is fast, it should be noticeable, yes?)
 - [ ] See if PascalScript can be used more liberally here.
 - [X] I need to see if I can use the debugger with Emacs `dap-mode`.

## Resources

 * https://github.com/Fr0sT-Brutal/awesome-pascal : Woah!
 * https://github.com/synopse/dmustache : Might be better Moustache implementation for templating.
 * Maybe anything mORMot, but not v2 yet, because I didn't find useful docs on it yet.

## Auto-formatting

`ptop` is used here. Use is `ptop -c ptop.cfg <infile> <outfile>`. Some richer details: https://wiki.freepascal.org/PTop.

Have an Emacs function to do this on save:

```
(setq ptop-cfg-file "ptop.cfg")
(defun format-with-ptop ()
  (shell-command  (concat "ptop -c " ptop-cfg-file " " (buffer-file-name) " " (buffer-file-name)))
  )
(add-hook 'opascal-mode-hook (lambda ()
                               (add-hook 'after-save-hook #'format-with-ptop 0 t)
                               ))
```

## Debugging

It's possible with GDB and `dap-mode`, but `dap-mode` is not perfect here yet. Currently requires a debug flag (using `fpc.cfg`, adding debug flags:

```
# generate always debugging information for GDB (slows down the compiling
# process)
#      -gc        generate checks for pointers
#      -gd        use dbx
#      -gg        use gsym
#      -gh        use heap trace unit (for memory leak debugging)
#      -gl        use line info unit to show more info for backtraces
#      -gv        generates programs tracable with valgrind
#      -gw        generate dwarf debugging info
```

(this is from the auto-generated config file.)

Loading it up via `dap-mode` requires digging up the binary under `lib/<arch>-<os>`. Maybe this could be made easier?

## fpTemplate

Is it good compared to moustache implementations?

 |-----------------------------------------------------------------------------------|------------------------------------------------------------------------------------------|
 | Pro                                                                               | Con                                                                                      |
 |-----------------------------------------------------------------------------------|------------------------------------------------------------------------------------------|
 | Do logic in Pascal.                                                               | Feels like to be really useful, it needs a wrapper library to implement common patterns. |
|                                                                                   |                                                                                          |
 |                                                                                   |                                                                                          |
 | No need for fancy template engine                                                 |                                                                                          |
 | Can change the the tag delimiters easily                                          |                                                                                          |
 | Making safe templates is easy (safe = not running arbitrary code, ala PHP or ERB. |                                                                                          |

Ideally, I should be able to pass into a function/procedure a dictionary and it renders it for me. Templates would get rendered by looking for special tags to swap out content from it:

This is my idea, but it is not implemented:

 1. Call `Render(Template: String; Data: TDictionary)`. The data dictionary would have to be generic in value. The template library would cast to known data structures dependent on the input.
 2. `<%=` would do a simple substitution. from a dictionary.
 3. `<%f {{l= mydata}} {{t= <row><col>[label1]</col><col>[label2]</col></row> }} %>` to mimic looping through a list where `label*` is the field name.
 4. `<%f` can have a no-data parameter : `<%f {{l= mydata}} {{t= ...}} {{e= "There was no data"}} %>`.

## License info

 - The code I have here is under the MIT license.
 - For Kumis: https://github.com/TandangDewe/kumis/blob/main/COPYING.LGPL
